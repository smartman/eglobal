<?php

$factory->define(App\Client::class, function (Faker\Generator $faker) {
    return [
        'id' => 5001,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName
    ];
});

$factory->define(App\Address::class, function (Faker\Generator $faker) {
    return [
        "client_id" => function() {
            return factory(\App\Client::class)->create()->id;
        },
        "city" => $faker->city,
        "street" => $faker->streetName,
        "country" => $faker->country,
        "zipcode" => $faker->postcode,
    ];
});
