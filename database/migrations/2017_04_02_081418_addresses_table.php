<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddressesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("addresses", function($t) {
            $t->engine = 'InnoDB';
            $t->increments("id");
            $t->string("country");
            $t->string("city");
            $t->string("street");
            $t->string("zipcode");
            $t->boolean("default")->default(FALSE);
            $t->integer("client_id")->unsigned();
            $t->foreign("client_id")->references("id")->on("clients");
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("addresses");
    }

}
