<?php

namespace App\Http\Controllers;

use App\Services\AddressService;
use App\Http\Requests\AddressRequest;
use App\Client;
use App\Address;

class AddressController extends Controller {

    protected $client;
    protected $aService;

    public function __construct(AddressService $aService) {
        $this->client = Client::find(1); //emulates logged in user
        $this->aService = $aService;
    }

    public function index() {
        $addresses = $this->client->addresses;
        return $addresses;
    }

    public function store(AddressRequest $request) {
        if (count($this->client->addresses) == 3) {
            info("Cannot add more addresses to " . $this->client->id);
            return response()->json(["error" => "Cannot add more addresses"], 400);
        }
        $address = $this->aService->make($request, $this->client);
        $address->save();
        return $address;
    }

    public function destroy($address_id) {
        $message = $this->aService->deleteAddress($address_id, $this->client);
        if ($message == null) {
            return response()->json(["message" => "Deleted"], 200);
        } else {
            return response()->json(["error" => $message], 403);
        }
    }

    public function makeDefault($address_id) {
        $message = $this->aService->makeDefault($address_id, $this->client);
        if ($message == null) {
            return response()->json(["message" => "Default changed"], 200);
        } else {
            return response()->json(["error" => $message], 403);
        }
    }

}
