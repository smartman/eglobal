<?php

namespace App\Services;

use App\Http\Requests\AddressRequest;
use App\Address;

class AddressService {

    public function make(AddressRequest $request, $client) {
        $address = new Address();
        if (count($client->addresses) == 0) {
            $address->default = true;
        }
        $address->city = $request->city;
        $address->street = $request->street;
        $address->zipcode = $request->zip_code;
        $address->country = $request->country;
        $address->client_id = $client->id;
        return $address;
    }

    public function makeDefault($addressId, $client) {
        $address = Address::where("client_id", $client->id)->where("id", $addressId)->first();
        if ($address === null) {
            return "No permission to edit this address or address not existing";
        }

        $addresses = $client->addresses;
        foreach ($addresses as $a) {
            $a->default = false;
            $a->save();
        }
        $address->default = true;
        $address->save();
        return null;
    }
    
        public function deleteAddress($addressId, $client) {
        $address = Address::where("client_id", $client->id)->where("id", $addressId)->first();
        if ($address === null) {
            return "No permission to edit this address or address not existing";
        }       
        if ($address->default) {
            return "Cannot delete default address";
        }
        $address->delete();
        return null;
    }

}
