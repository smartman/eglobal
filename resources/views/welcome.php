<!DOCTYPE html>
<html>
    <head>
        <title>E-Global</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.js" integrity="sha256-+f0njwC9E3IT9zDPry5DSIcGdSxQYezBaStQ4L0ZRfU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular-animate.js" integrity="sha256-BBgufnyROWe7BGMx7tGTfMyfPh/SKbEiz/3M51IDID4=" crossorigin="anonymous"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/superhero/bootstrap.min.css" rel="stylesheet" integrity="sha384-Xqcy5ttufkC3rBa8EdiAyA1VgOGrmel2Y+wxm4K3kI3fcjTWlDWrlnxyD6hOi3PF" crossorigin="anonymous">
        <link href="/css/app.css" rel="stylesheet">
    </head>
    <body>
        <div class="container top40" ng-app="AddressApp" ng-controller="ClientController">
            <div class="jumbotron">     
                <div ng-show="addErrorMessage" class="alert alert-danger animate-show-hide">{{errorMessage}}</div>
                <p>Logged in user:</p>
                <h1>{{client.firstname+" "+client.lastname}}</h1>
            </div>
            <p>Addresses:</p>                
            <div ng-if="addresses.length == 0" class="alert alert-danger"> No addresses</div>
            <div class="row">
                <div ng-repeat="address in addresses" class="col-md-4"> 
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <span ng-show="address.default == 1">Default</span> <a ng-show="address.default == 0" ng-click="makeDefault(address.id)" class="alert-info">Make default</a>
                        </div>
                        <div class="panel-body">
                            <address>
                                <strong>{{address.city}}</strong><br>
                                {{address.street}}<br>
                                {{address.zipcode}}<br>
                                {{address.country}}
                            </address>    
                            <a ng-show="address.default == 0" ng-click="deleteAddress(address.id)" >Delete</a>
                        </div>
                    </div>
                </div>
            </div>         

            <div ng-show="showNewAddressMessage" class="alert alert-success animate-show-hide">New address added</div>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addressModal">
                Add new Address
            </button>

            <!-- Modal -->
            <div class="modal fade" id="addressModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <form name="newAddressForm" class="form-horizontal" role="form" ng-submit="submitNewAddress()">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add new address</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <!-- Form Name -->
                                            <legend>Address Details</legend>                                            

                                            <!-- Text input-->
                                            <div class="form-group" ng-class="{ 'has-error': newAddressForm.city.$invalid && !newAddressForm.city.$pristine }">
                                                <label class="col-sm-2 control-label" for="textinput">City</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="city" placeholder="City" class="form-control" ng-model="formData.city" required ng-minlength="2" ng-maxlength="100">                                                        
                                                    <p class="help-block" ng-show="newAddressForm.city.$invalid && !newAddressForm.city.$pristine">Please make sure it is valid city name</p>
                                                </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group" ng-class="{ 'has-error': newAddressForm.street.$invalid && !newAddressForm.street.$pristine }">
                                                <label class="col-sm-2 control-label">Street</label>
                                                <div class="col-sm-10">
                                                    <input name="street" type="text" placeholder="Street" class="form-control" ng-model="formData.street" required ng-minlength="2" ng-maxlength="100">
                                                    <p class="help-block" ng-show="newAddressForm.street.$invalid && !newAddressForm.street.$pristine">Please make sure it is valid street name</p>
                                                </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group" ng-class="{ 'has-error': newAddressForm.zip_code.$invalid && !newAddressForm.zip_code.$pristine }">
                                                <label class="col-sm-2 control-label">Zip/Postal code</label>
                                                <div class="col-sm-10">
                                                    <input name="zip_code" type="text" placeholder="Zip/Postal code" class="form-control" ng-model="formData.zip_code" required ng-minlength="2" ng-maxlength="100">
                                                    <p class="help-block" ng-show="newAddressForm.zip_code.$invalid && !newAddressForm.zip_code.$pristine">Please make sure it is valid zip code</p>
                                                </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Country</label>                                                    
                                                <div class="col-sm-10">
                                                    <select name="country" id="country" class="form-control" ng-model="formData.country" required="" ng-options="item for item in countries">                                                            

                                                    </select>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </div><!-- /.col-lg-12 -->
                                </div><!-- /.row -->
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" ng-disabled="newAddressForm.$invalid">Save address</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="/js/app.js" type="text/javascript"></script>
    </body>
</html>

