<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\AddressService;
use App\Address;

class ServiceTest extends TestCase {

    use DatabaseTransactions;

    protected $service;

    public function __construct() {
        $this->service = new AddressService();
    }

    public function testMake() {
        $client = factory(\App\Client::class)->create();
        $request = new \App\Http\Requests\AddressRequest();
        $address = $this->service->make($request, $client);
        $this->assertInstanceOf("App\Address", $address);
    }

    public function testMakeDefault() {
        $address = factory(\App\Address::class)->create(["default" => FALSE]);
        $client = $address->client;
        $this->assertEquals(0, Address::find($address->id)->default);
        $this->service->makeDefault($address->id, $client);
        $this->assertEquals(1, Address::find($address->id)->default);
    }

    public function testDelete() {
        $address = factory(\App\Address::class)->create(["default" => FALSE]);
        $client = $address->client;        
        $this->service->deleteAddress($address->id, $client);
        $this->assertNull(Address::find($address->id));
    }

    public function testDeleteFailed() {
        $address = factory(\App\Address::class)->create(["default" => TRUE]);
        $client = $address->client;
        $this->assertEquals(1, Address::find($address->id)->default);
        $this->service->deleteAddress($address->id, $client);
        $this->assertInstanceOf("App\Address", Address::find($address->id));        
    }

}
