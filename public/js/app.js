var app = angular.module('AddressApp', ['ngAnimate']);
app.controller('ClientController', function ($scope, $http, $timeout) {

    $scope.loadAdresses = function () {
        $http.get("/api/current_client")
                .then(function (response) {
                    $scope.client = response.data;
                    $http.get("/api/address")
                            .then(function (response) {
                                $scope.addresses = response.data;
                            });
                });
    };
    $scope.loadAdresses();

    $scope.makeDefault = function (id) {
        $http.get("/api/make_default/" + id)
                .then(function (response) {
                    $scope.loadAdresses();
                }, function (response) {
                    $scope.showError(response.data.error);
                });
    };

    $scope.deleteAddress = function (id) {
        $http.delete("/api/address/" + id)
                .then(function (response) {
                    $scope.loadAdresses();
                }, function (response) {
                    $scope.showError(response.data.error);
                });
    };

    $http.get("/api/countries")
            .then(function (response) {
                $scope.countries = response.data;
            });

    $scope.showError = function (message) {
        $scope.addErrorMessage = true;
        $scope.errorMessage = message;
        $timeout(function () {
            $scope.addErrorMessage = false;
        }, 5000);
    };

    $scope.formData = {"country": "Latvia"};

    $scope.submitNewAddress = function () {
        $http({
            method: 'POST',
            url: '/api/address',
            data: $.param($scope.formData),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (data) {
            console.log(data);
            if (!data.success) {
                // if not successful, bind errors to error variables
                $('#addressModal').modal('hide');
                $scope.showNewAddressMessage = true;
                $timeout(function () {
                    $scope.showNewAddressMessage = false;
                }, 5000);
                $scope.loadAdresses();
            } else {
                console.log(data.error);
            }
        }, function (data) {
            $('#addressModal').modal('hide');
            $scope.showError(data.data.error);
        });
    };
});